package com.study.mobile.server.tts;

import java.util.Locale;

public enum Voices {
    SPIKE("dfki-spike-hsmm", Gender.MALE, "en_GB"), // real
    MARY("dfki-prudence-hsmm", Gender.FEMALE, "en_GB"), // good
    REMBO("cmu-rms-hsmm", Gender.MALE, "en_GB"), // perfect
    TOLYA("voxforge-ru-nsh", Gender.MALE, "ru"), // russiano
    PETYA("voxforge-ru-nsh", Gender.MALE, "ru"); // ukr

    private final String voiceName;
    private final Gender gender;
    private final String locale;

    private Voices(String voiceName, Gender gender, String locale) {
        this.voiceName = voiceName;
        this.gender = gender;
        this.locale = locale;
    }
    public String getVoiceName() {
        return voiceName;
    }

    public Gender getGender() {
        return gender;
    }

    public String getLocale() {
        return locale;
    }

    private enum Gender {
        MALE, FEMALE;
    }
}