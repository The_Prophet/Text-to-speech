package com.study.mobile.server.tts;

import marytts.LocalMaryInterface;
import marytts.MaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;
import marytts.modules.synthesis.Voice;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Locale;

public class TextToSpeech {

    private static final Voices DEFAULT_VOICE = Voices.PETYA;

    private static final Logger LOG = LoggerFactory.getLogger(TextToSpeech.class);
    public static final int FILE_NAME_LENGTH = 7;

    private MaryInterface marytts;

    public TextToSpeech() {
        try {
            marytts = new LocalMaryInterface();
            marytts.setVoice(DEFAULT_VOICE.getVoiceName());
        } catch (MaryConfigurationException ex) {
            LOG.error("Can not create LocalMaryInterface", ex);
            throw new RuntimeException(ex);
        }
    }

    public Collection<Voice> getAvailableVoices() {
        return Voice.getAvailableVoices();
    }

    public void setVoice(Voices voice) {
        marytts.setVoice(voice.getVoiceName());

        if ("ru".equals(voice.getLocale())) {
            marytts.setLocale(new Locale(voice.getLocale()));
        }
    }

    public String getVoice() {
        return marytts.getVoice();
    }

    public String convertToAudio(String text, String path, AudioFileFormat.Type fileFormat) {

        if (StringUtils.isEmpty(text)) {
            throw new RuntimeException("Text can not be empty");
        }

        if (!new File(path).exists()) {
            LOG.error("Path should exist: " + path);
            throw new RuntimeException("Path should exists");
        }

        String fileName = generateFileName(fileFormat);

        try (AudioInputStream audioStream = marytts.generateAudio(text)) {

            try(FileOutputStream fout = new FileOutputStream(path + fileName)) {
                AudioSystem.write(audioStream, fileFormat, fout);
            }
        } catch (SynthesisException e) {
            LOG.error("Can not generate AudioStream from text", e);
            throw new RuntimeException(e);
        } catch (IOException e) {
            LOG.error("Error while creating file", e);
            throw new RuntimeException(e);
        }
        return fileName;
    }

    protected String generateFileName(AudioFileFormat.Type type) {
        return RandomStringUtils.randomAlphabetic(FILE_NAME_LENGTH) + "." + type.getExtension();
    }
}