package com.study.mobile.server.utils;

import java.util.LinkedHashMap;
import java.util.Map;

public class TextUtils {

    private static final Map<String, String> DICTIONARY = new LinkedHashMap<>();

    static {
        DICTIONARY.put("и", "ы");
        DICTIONARY.put("і", "и");
        DICTIONARY.put("ї", "и");
        DICTIONARY.put("є", "е");
        DICTIONARY.put("ґ", "г");
        DICTIONARY.put("И", "Ы");
        DICTIONARY.put("І", "И");
        DICTIONARY.put("Ї", "И");
        DICTIONARY.put("Є", "Е");
        DICTIONARY.put("Ґ", "Г");
    }

    public static String replaceUkrainianLetters(String input) {

        for (Map.Entry<String, String> entry : DICTIONARY.entrySet()) {
            input = input.replace(entry.getKey(), entry.getValue());
        }
        return input;
    }
}
