package com.study.mobile.server.utils;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class ResourceCleaner implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(ResourceCleaner.class);

    private final File directory;

    public ResourceCleaner(String path) {
        directory = new File(path);
    }

    @Override
    public void run() {
        try {
            LOG.debug("Cleaning " + directory.getAbsolutePath());
            FileUtils.cleanDirectory(directory);
        } catch (IOException e) {
            LOG.error("Can not clean directory: " + directory.getAbsolutePath(), e);
        }
    }
}
