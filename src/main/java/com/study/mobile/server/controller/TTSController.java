package com.study.mobile.server.controller;

import com.study.mobile.server.tts.TextToSpeech;
import com.study.mobile.server.tts.Voices;
import com.study.mobile.server.utils.ResourceCleaner;
import com.study.mobile.server.utils.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.sound.sampled.AudioFileFormat;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@RestController
public class TTSController {

    private static final String DEFAULT_ENCODING = "UTF-8";
    private static final AudioFileFormat.Type DEFAULT_FILE_FORMAT = AudioFileFormat.Type.WAVE;
    private static final int CLEANING_PERIOD_IN_SECONDS = 60;

    private static final Logger LOG = LoggerFactory.getLogger(TTSController.class);

    @RequestMapping("/")
    public String index(@RequestParam("text") String text, @RequestParam(name="voice", required = false) String voice) {
        TextToSpeech textToSpeech = new TextToSpeech();

        if (voice != null) {
            Voices newVoice = Voices.valueOf(voice);

            textToSpeech.setVoice(newVoice);
        }

        if (textToSpeech.getVoice().equals(Voices.PETYA.getVoiceName())) {
            text = TextUtils.replaceUkrainianLetters(text);
        }

        String resourcesLocation = System.getProperty("user.dir") + "/audio/";
        String fileName;

        try {
            fileName  = textToSpeech.convertToAudio(text, resourcesLocation, DEFAULT_FILE_FORMAT);
            scheduleCleaning(resourcesLocation, CLEANING_PERIOD_IN_SECONDS);
        } catch (Exception e) {
            LOG.error("error----", e);
            fileName = "error";
        }
        try {
            fileName = URLEncoder.encode(fileName, DEFAULT_ENCODING).replace("+", "%20");
            LOG.debug("Response [" + fileName + "]");
        } catch (UnsupportedEncodingException e) {
            LOG.error("Error", e);
        }

        return fileName;
    }

    @RequestMapping("/voices")
    public String[] getAvailableVoices() {

        Voices[] values = Voices.values();
        String[] names = new String[values.length];

        for (int i = 0; i < values.length; ++i) {
            names[i] = values[i].name();
        }

        return names;
    }

    private void scheduleCleaning(String path, int periodInSeconds) {
        ResourceCleaner rc = new ResourceCleaner(path);

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(rc, periodInSeconds, periodInSeconds, TimeUnit.SECONDS);
    }
}