package com.study.mobile.server.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

@RestController
public class FileController {

    @RequestMapping(value = "/audio/{name}.{extension}")
    public void allImage(@PathVariable String name, @PathVariable String extension, HttpServletResponse response)
            throws IOException {
        String fileName = name + "." + extension;

        System.out.println("FileController File Name: " + fileName);

        String fullPath = System.getProperty("user.dir") + "/audio/" + fileName;

        InputStream is = new FileInputStream(new File(fullPath));
        BufferedInputStream inputStream = new BufferedInputStream(is);
        int nRead;
        byte[] b = new byte[10000];
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        while ((nRead = inputStream.read(b, 0, b.length)) != -1) {
            arrayOutputStream.write(b, 0, nRead);
        }
        arrayOutputStream.flush();
        byte[] bn = arrayOutputStream.toByteArray();
        response.setContentType("audio/mpeg");
        response.setContentLength(bn.length);
        OutputStream os = response.getOutputStream();
        os.write(bn);
        os.flush();
        os.close();
        inputStream.close();
    }
}